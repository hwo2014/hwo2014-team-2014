package noobbot.data;

public class PiecePosition {

	public int pieceIndex;
	public double inPieceDistance;
	public int startLaneIndex;
	public double endLaneIndex;
	public double lap;

	
	public String string() {
		StringBuffer sb = new StringBuffer();
		sb.append("pieceIndex: " + pieceIndex + "\n");
		sb.append("inPieceDistance: " + inPieceDistance + "\n");
		sb.append("startLaneIndex: " + startLaneIndex + "\n");
		sb.append("endLaneIndex: " + endLaneIndex + "\n");
		sb.append("lap: " + lap + "\n");
		return sb.toString();
	}
}
