package noobbot.data;

public class Track {

	public String id;
	public String name;
	public Piece[] pieces;
	public Lane[] lanes;
	public Car[] cars;
	public RaceSession raceSession;

}
