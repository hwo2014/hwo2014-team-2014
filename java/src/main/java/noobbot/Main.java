package noobbot;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

import noobbot.data.CarPosition;
import noobbot.data.Track;
import noobbot.logic.LogicResult;
import noobbot.logic.RaceLogic;
import noobbot.logic.Statistics;
import noobbot.logic.TickData;
import noobbot.reader.RaceReader;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.NumberTickUnit;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer;
    
    private Track track;
    private RaceReader raceReader = new RaceReader();
    private RaceLogic raceLogic = new RaceLogic();
    private LogicResult logicResult = new LogicResult();
    boolean sent = false;
    
    private long lastTime = 0;
    
    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;

        send(join);

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
            	
            	CarPosition[] cps = raceReader.readCarPositions((ArrayList<Object>) msgFromServer.data);
            	
            	raceLogic.calculateThrottle(join.name, cps, this.track, logicResult);
            	
//            	send(new SwitchLane("Right"));
  //              send(new Throttle(logicResult.throttle));
                
                //if(!sent/*logicResult.suitch != 0*/) {
//                	System.out.println("switch " + (logicResult.suitch < 0 ? "Left" : "Right"));
//                	send(new SwitchLane(logicResult.suitch < 0 ? "Left" : "Right"));
                	sent = true;
                //}
                	
                	if(logicResult.suitch == 0) {
                		send(new Throttle(logicResult.throttle));
                	} else {
//                    	System.out.println("switch " + (logicResult.suitch < 0 ? "Left" : "Right"));
                		send(new SwitchLane(logicResult.suitch < 0 ? "Left" : "Right"));
                	}
                	
                	
                	long currentTime = System.currentTimeMillis();
                	if(currentTime - lastTime > 3000) {
                		exportPng();
                    	lastTime = currentTime;
                	}
                	
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("crash")) {
                System.out.println("Crashed");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
                this.track = raceReader.readRaceData((LinkedTreeMap<String, Object>) msgFromServer.data);
                raceLogic.initialize(track);
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else {
                send(new Ping());
            }
        }
    }



	private void exportPng() throws IOException, FileNotFoundException {
		
		System.out.println("Export PNG.");
		
		Statistics statistics = raceLogic.statistics;
		
		XYSeriesCollection dataset = new XYSeriesCollection();
		XYSeries throttle = new XYSeries("throttle");
		XYSeries suitch = new XYSeries("switch");
		XYSeries velocity = new XYSeries("velocity");
		XYSeries radius = new XYSeries("radius");
		XYSeries offset = new XYSeries("offset");
		for(int i = 0; i < statistics.tickDatas.size(); i++) {
			TickData tickData = statistics.tickDatas.get(i);
			throttle.add(tickData.tick, tickData.throttle);
			suitch.add(tickData.tick, tickData.suitch + 2.0);
			velocity.add(tickData.tick, tickData.velocity);
			radius.add(tickData.tick, Math.signum(tickData.radius) + 4.0);
			offset.add(tickData.tick, Math.signum(tickData.offset) + 6.0);
		}
		dataset.addSeries(throttle);
		dataset.addSeries(suitch);
		dataset.addSeries(velocity);
		dataset.addSeries(radius);
		dataset.addSeries(offset);
		JFreeChart chart = ChartFactory.createXYLineChart("", "", "", dataset, PlotOrientation.VERTICAL, true, false, false);
		
		
		NumberAxis xAxis = new NumberAxis();
		xAxis.setTickUnit(new NumberTickUnit(1));

		// Assign it to the chart
		XYPlot plot = chart.getXYPlot();
		plot.setDomainAxis(xAxis);
		
		BufferedImage bufferedImage = chart.createBufferedImage(640 * 5, 480);
		byte[] bytes = ChartUtilities.encodeAsPNG(bufferedImage);
		File file = new File("/Users/Shared/helloworldopen/hwo2014-team-2014/java/graph.png");
		FileOutputStream fileOutputStream = new FileOutputStream(file);
		fileOutputStream.write(bytes);
		fileOutputStream.flush();
		fileOutputStream.close();
	}



    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class SwitchLane extends SendMsg {
    private String value;

    public SwitchLane(String value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}