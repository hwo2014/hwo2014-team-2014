package noobbot.reader;

import java.util.ArrayList;

import noobbot.data.Car;
import noobbot.data.CarPosition;
import noobbot.data.Lane;
import noobbot.data.Piece;
import noobbot.data.PiecePosition;
import noobbot.data.RaceSession;
import noobbot.data.StartingPoint;
import noobbot.data.Track;

import com.google.gson.internal.LinkedTreeMap;

public class RaceReader {

	public Track readRaceData(LinkedTreeMap<String, Object> data) {
		LinkedTreeMap<String, Object> race = (LinkedTreeMap<String, Object>) data.get("race");
		LinkedTreeMap<String, Object> track = (LinkedTreeMap<String, Object>) race.get("track");
		Track t = new Track();
		t.id = (String) track.get("id");
		t.name = (String) track.get("name");
		
		ArrayList<Object> pieces = (ArrayList<Object>) track.get("pieces");
		t.pieces = new Piece[pieces.size()];
		for(int i = 0; i < t.pieces.length; i++) {
			LinkedTreeMap<String, Object> piece = (LinkedTreeMap<String, Object>) pieces.get(i);
			Piece p = new Piece();
			boolean length = false;
			if(piece.get("length") != null) {
				length = true;
				p.length = (Double) piece.get("length");
			}
			boolean suitch = false;
			if(piece.get("switch") != null) {
				suitch = true;
				p.suitch = (Boolean) piece.get("switch");
			}
			boolean angle = false;
			if(piece.get("angle") != null) {
				angle = true;
				p.angle = (Double) piece.get("angle");
			}
			boolean radius = false;
			if(piece.get("radius") != null) {
				radius = true;
				p.radius = (Double) piece.get("radius");
			}
			
			
			if(radius && angle) {
				p.type = Piece.TYPE_CURVE;
			} else if(length) {
				p.type = Piece.TYPE_STRAIGHT;
			} else {
				throw new IllegalStateException();
			}
			
			p.suitch = suitch;
			
			t.pieces[i] = p;
		}
		
		ArrayList<Object> lanes = (ArrayList<Object>) track.get("lanes");
		t.lanes = new Lane[lanes.size()];
		for(int i = 0; i < t.lanes.length; i++) {
			LinkedTreeMap<String, Object> lane = (LinkedTreeMap<String, Object>) lanes.get(i);
			Lane l = new Lane();
			l.distanceFromCenter = (Double) lane.get("distanceFromCenter");
			l.index = (Double) lane.get("index");
			t.lanes[i] = l;
		}
	
		LinkedTreeMap<String, Object> startingPoint = (LinkedTreeMap<String, Object>) track.get("startingPoint");
		LinkedTreeMap<String, Object> position = (LinkedTreeMap<String, Object>) startingPoint.get("position");
		StartingPoint sp = new StartingPoint();
		sp.x = (Double) position.get("x");
		sp.y = (Double) position.get("x");
		sp.angle = (Double) startingPoint.get("angle");

		ArrayList<Object> cars = (ArrayList<Object>) race.get("cars");
		t.cars = new Car[cars.size()];
		for(int i = 0; i < t.cars.length; i++) {
			LinkedTreeMap<String, Object> car = (LinkedTreeMap<String, Object>) cars.get(i);
			
			LinkedTreeMap<String, Object> id = (LinkedTreeMap<String, Object>) car.get("id");
			LinkedTreeMap<String, Object> dimensions = (LinkedTreeMap<String, Object>) car.get("dimensions");

			
			Car c = new Car();
			c.name = (String) id.get("name");
			c.color = (String) id.get("color");
			c.length = (Double) dimensions.get("length");
			c.width = (Double) dimensions.get("width");
			c.guideFlagPosition = (Double) dimensions.get("guideFlagPosition");
			t.cars[i] = c;
		}
		
		LinkedTreeMap<String, Object> raceSession = (LinkedTreeMap<String, Object>) race.get("raceSession");
		RaceSession rs = new RaceSession();
		rs.laps =  (Double) raceSession.get("laps");
		rs.maxLapTimeMs =  (Double) raceSession.get("maxLapTimeMs");
		rs.quickRace =  (Boolean) raceSession.get("quickRace");
		t.raceSession = rs;
		 
		return t;
	}

	public CarPosition[] readCarPositions(ArrayList<Object> data) {
		
		CarPosition[] cps = new CarPosition[data.size()];
		
		for(int i = 0; i < cps.length; i++) {
			
			LinkedTreeMap<String, Object> carPosition = (LinkedTreeMap<String, Object>) data.get(i);
			
			
			LinkedTreeMap<String, Object> id = (LinkedTreeMap<String, Object>) carPosition.get("id");
			
			CarPosition cp = new CarPosition();
			cp.name = (String) id.get("name");
			cp.color =  (String) id.get("color");
			cp.angle = (Double) carPosition.get("angle");
			
			PiecePosition pp = new PiecePosition();
			LinkedTreeMap<String, Object> piecePosition = (LinkedTreeMap<String, Object>) carPosition.get("piecePosition");

			pp.pieceIndex = (int) ((Double) piecePosition.get("pieceIndex")).doubleValue();
			pp.inPieceDistance = (Double) piecePosition.get("inPieceDistance");
			
			LinkedTreeMap<String, Object> lane = (LinkedTreeMap<String, Object>) piecePosition.get("lane");
			pp.startLaneIndex = (int) ((Double) lane.get("startLaneIndex")).doubleValue();
			pp.endLaneIndex = (int) ((Double) lane.get("endLaneIndex")).doubleValue();

			pp.lap = (Double) piecePosition.get("lap");
			cp.piecePosition = pp;
			
//			System.out.println("=== cp ===");
//			System.out.println(pp.string());
			cps[i] = cp;
		}

				
		return cps;
	}
}
