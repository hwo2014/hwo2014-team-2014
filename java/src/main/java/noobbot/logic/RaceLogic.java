package noobbot.logic;

import java.util.ArrayList;

import noobbot.data.CarPosition;
import noobbot.data.Piece;
import noobbot.data.PiecePosition;
import noobbot.data.Track;

public class RaceLogic {

	private double velocity;
	private PiecePosition lastPiecePosition = null;
	private SwitchData switchData;
	public Statistics statistics;
	private int tick; 

	public void initialize(Track track) {
		this.velocity = 0.0;
		lastPiecePosition = null;


		int index = 0;
		this.switchData = calculateOptimum(track, index);
		
		
		Statistics statistics = new Statistics();
		statistics.tickDatas = new ArrayList<TickData>();
		
		this.statistics = statistics;
		
		tick = 0;
	}




	private SwitchData calculateOptimum(Track track, int index) {

		int laneCount = track.lanes.length;

		int switchCount = 0;
		for(int i = 0; i < track.pieces.length; i++) {
			Piece p = track.pieces[i];
			if(p.suitch) {
				switchCount++;
			}
		}
		
		SwitchData switchData = new SwitchData();
		if(switchCount == 0) {
			switchData.hasSwitch = false;
			return switchData;
		} else {
			switchData.hasSwitch = true;
		}

		SwitchSector[] switchSectors = new SwitchSector[switchCount];

		for(int i = 0; i < switchCount; i++) {

			SwitchSector switchSector = new SwitchSector();
			switchSector.switchPieceIndex = -1;
			switchSector.laneLengths = new double[laneCount];
			switchSector.laneRanks = new int[laneCount];

			for(int l = 0; l < laneCount; l++) {
				switchSector.laneLengths[l] = 0.0;
				switchSector.laneRanks[l] = - 1;
			}
			
			switchSectors[i] = switchSector;

		}

		int switchSectorIndex = switchSectors.length - 1;
		for(int i = 0; i < track.pieces.length; i++) {

			//System.out.println("piece " + i);
			
			Piece p = track.pieces[i];

			if(p.suitch) {
				switchSectorIndex = (switchSectorIndex + 1) % switchSectors.length;
				switchSectors[switchSectorIndex].switchPieceIndex = i;
			}


			SwitchSector ss = switchSectors[switchSectorIndex];
			for(int l = 0; l < laneCount; l++) {
				double length = length(track, track.pieces[i], l);
				//System.out.println("length " + l + " = " + length);
				ss.laneLengths[l] += length;
			}

		}

		for(int s = 0; s < switchSectors.length; s++) {
			SwitchSector ss = switchSectors[s];

			for(int r = 0; r < laneCount; r++) {
				double minLength = 0.0;
				int minIndex = - 1;
				for(int i = 0; i < laneCount; i++) {
					if(ss.laneRanks[i] == -1) {
						double length = ss.laneLengths[i];
						if(minIndex == - 1 || length < minLength) {
							minLength = length;
							minIndex = i;
						}
					}
				}
				if(minIndex == - 1) {
					minIndex = laneCount - 1;
				}
				ss.laneRanks[minIndex] = r;
			}

		}
		
		switchData.switchSectors = switchSectors;
		
		return switchData;
		
	}


	public void calculateThrottle(String name, CarPosition[] cps, Track track, LogicResult logicResult) {
		
		TickData tickData = new TickData();
		tickData.tick = tick;
		tick++;
		
		double throttle = 1.0;

		double maxForce = 0.4;

		logicResult.suitch = 0;

		for(int i = 0; i < cps.length; i++) {
			CarPosition cp = cps[i];
			if(cp.name.equals(name)) {
				
//				if(Math.abs(cp.angle) > 1.0) {
//					System.out.println("angle " + cp.angle);
//				}
				
				Piece pi = track.pieces[cp.piecePosition.pieceIndex];
				if(pi.type == Piece.TYPE_CURVE) {
					tickData.radius = pi.radius;
				} else {
					tickData.radius = 0.0;
				}


				if(lastPiecePosition != null) {

					int lastIndex = lastPiecePosition.pieceIndex;
					int currentIndex = cp.piecePosition.pieceIndex;
					Piece lastPiece = track.pieces[lastPiecePosition.pieceIndex];

					double delta = 0.0;
					if(lastIndex == currentIndex) {
						delta = cp.piecePosition.inPieceDistance - lastPiecePosition.inPieceDistance;
					} else if(currentIndex == (lastIndex + 1) % track.pieces.length) {

						double length = length(track, lastPiece);

						delta = cp.piecePosition.inPieceDistance + (length - lastPiecePosition.inPieceDistance);
					} else {
						throw new IllegalStateException("lastIndex " + lastIndex + ", currentIndex " + currentIndex);
					}

					this.velocity = delta;
				}

				lastPiecePosition = cp.piecePosition;


				if(track.pieces[cp.piecePosition.pieceIndex].type == Piece.TYPE_CURVE) {
					//throttle = 0.5;
					//
					//					double force = 1.0 * this.velocity * this.velocity / radius;

					double radius = radius(track, track.pieces[cp.piecePosition.pieceIndex]);

					double maxVelocity = Math.sqrt(maxForce * radius);

					if(this.velocity > maxVelocity /*|| (Math.abs(cp.angle) > 30 && this.velocity > 1.0)*/) {
						throttle = 0.2;
					} else {
						throttle = 1.0;
					}


					//System.out.println("force: " + force);
				} else {

					double offset = length(track, track.pieces[cp.piecePosition.pieceIndex]) - cp.piecePosition.inPieceDistance;
					Piece nextCurve = null;
					for(int j = 1; j < 10; j++) {
						int nextIndex = (cp.piecePosition.pieceIndex + j) % track.pieces.length;        			
						Piece p = track.pieces[nextIndex];
						if(p.type == Piece.TYPE_CURVE) {
							nextCurve = p;
							break;
						}
						offset += length(track, track.pieces[nextIndex]);
					}
					if(nextCurve == null) {
						throttle = 1.0;
					} else {

						double radius = radius(track, nextCurve);

						double maxVelocity = Math.sqrt(maxForce * radius);
						if(offset < 200.0) {
							tickData.offset = 1.0;
							
							if(this.velocity > maxVelocity) {
								throttle = 0.0;
							}
						} else {
							tickData.offset = 0.0;
						}

						//						double force = 1.0 * this.velocity * this.velocity / radius;
						//						if(force > 0.5) 
						//						throttle = 0.0;
					}
					//System.out.println("offset " + offset);
				}

				
				
				int nextIndex = (cp.piecePosition.pieceIndex + 1) % track.pieces.length;
				Piece nextPiece = track.pieces[nextIndex];
				if(nextPiece.suitch && switchData.hasSwitch) {
					SwitchSector[] sss = switchData.switchSectors;
					for(int s = 0; s < sss.length; s++) {
						SwitchSector ss = sss[s];
						if(ss.switchPieceIndex == nextIndex) {
							
							int winningLane = - 1;
							for(int r = 0; r < ss.laneRanks.length; r++) {
								if(ss.laneRanks[r] == 0) {
									winningLane = r;
								}
							}
							
							if(cp.piecePosition.endLaneIndex != winningLane) {
								if(winningLane > cp.piecePosition.endLaneIndex) {
									logicResult.suitch = 1;									
								} else if(winningLane < cp.piecePosition.endLaneIndex) {
									logicResult.suitch = - 1;
								} else {
									logicResult.suitch = 0;
								}
							}
							
						}
					}
				}
				
			}

		}

		logicResult.throttle = throttle;
		
		tickData.throttle = logicResult.throttle;
		tickData.suitch = logicResult.suitch;
		tickData.velocity = velocity;
		
		statistics.tickDatas.add(tickData);
		
//		logicResult.suitch = 0;
	}




	private double length(Track track, Piece piece, int laneIndex) {
		double length = 0.0;
		if(piece.type == Piece.TYPE_STRAIGHT) {
			length = piece.length;
		} else {

			double radius = radius(track, piece, laneIndex);

			double extent = 2 * Math.PI * radius;
			length = extent * Math.abs(piece.angle) / 360.0;

		}
		return length;
	}

	private double radius(Track track, Piece piece) {
		return radius(track, piece, lastPiecePosition.startLaneIndex);
	}

	private double length(Track track, Piece piece) {
		return length(track, piece, lastPiecePosition.startLaneIndex);
	}


	private double radius(Track track, Piece piece, int laneIndex) {
		double deltaRadius = 0.0;
		for(int l = 0; l < track.lanes.length; l++) {
			if(laneIndex == track.lanes[l].index) {
				deltaRadius = track.lanes[l].distanceFromCenter;
			}
		}

		double radius = piece.radius - deltaRadius * Math.signum(piece.angle);
		return radius;
	}


}
